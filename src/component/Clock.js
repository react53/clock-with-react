import React from 'react';

 class Clock extends React.Component{
  constructor(props){
  super(props);



  this.state = {
    Month : new Date().getMonth(),
    Day : new Date().getDay(),
    time : new Date().toLocaleTimeString(),
  };
};

componentDidMount(){
  this.timer = setInterval(()=> {
    this.setState({time : new Date().toLocaleTimeString()})
  } , 1000) ;

}

render(){
  return(
    <div>
      <h1>{this.state.Month} / {this.state.Day} </h1>
      <h3>{this.state.time}</h3>
    </div>
  )
}
}

export default Clock;
